<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rolUsuario = auth()->user()->rol;
        if ($rolUsuario=="Profesor") {
            $users = User::where('rol','Alumno');
            return view('home');
        }else {
           // die(print_r($rolUsuario));
        }
        
        
    }
}
