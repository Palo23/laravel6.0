@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <span>Lista de Notas para {{auth()->user()->name}}</span>
                    <a href="/notas/create" class="btn btn-primary btn-sm">Nueva Nota</a>
                </div>

                <div class="card-body">      
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Carnet de Alumno</th>
                            <th scope="col">Nombre de Alumno</th>
                            <th scope="col">Nota 1</th>
                            <th scope="col">Nota 2</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $alumnos)
                            <tr>
                                <th scope="row">
                                    <select>
                                    <option>{{$alumnos->carnetAlumno}}</option>
                                    </select>
                                </th>
                                <td>{{ $alumnos->nombre }}</td>
                                <td>{{ $alumnos->rol }}</td>
                                <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
