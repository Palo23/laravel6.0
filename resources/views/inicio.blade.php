@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <span>Lista de Notas para {{auth()->user()->name}}</span>
                    <a href="/notas/create" class="btn btn-primary btn-sm">Nueva Nota</a>
                </div>

                <div class="card-body">      
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Numero de Nota</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $alumnos)
                            <tr>
                                <th scope="row">
                                    <select>
                                    <option>{{$alumnos->carnetAlumno}}</option>
                                    </select>
                                </th>
                                <td>{{ $alumnos->nombre }}</td>
                                <td>{{ $alumnos->rol }}</td>
                                <td>
                                <a href="{{ route('notas.edit', $item) }}" 
                                class="btn btn-warning btn-sm">Editar</a>
                                <form action="{{ route('notas.destroy', $item) }}" method="POST" class="d-inline">
                                    @method('DELETE')
                                    @csrf
                                <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                                </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$notas->links()}}
                {{-- fin card body --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
